# Ryan - April 28, 2019

## Installation
  - clone repo
  - in project directory:
    - run `yarn`
    - run `docker-compose up --build`
    - run `yarn watch` to observe the src folder and rebuild on changes to files

## Features

## Security
  - the review app is generated on a ec2 instance, and the security group is restrictive but could be better analyzed.

## Improvements
  - completing all requirements
  - the ability to host multiple review apps